import type {Runtime} from "wxt/browser";
import {SETTINGS, manager} from "@/lib/storage/StorageManager";
import {getConfigForHost} from "@/lib/utils/PageService";
import {getCurrentSavePage} from "@/lib/utils/PageService";
import {createAlarm} from "@/lib/utils/AlarmService";
import {checkNextPage} from "@/lib/analysers/AnalyserService";

export default defineBackground({
    main() {
        // installation et gestion bdd
        browser.runtime.onInstalled.addListener(async function (details: any) {
            manager.addEventListener("db:ready", async () => {
                if (details.reason == "install") {
                    const response = await fetch(`${SETTINGS.CONFIG_URL}hosts.json?inline=false`);
                    const parsersConfigs = await response.json() as IHostSetting[]
                    await manager.hosts.saveAll(parsersConfigs)
                }
            })

            // Create an alarm so we have something to look at in the demo
            await createAlarm()
        });

        // event messaging
        browser.runtime.onMessage.addListener((msg: IMessageAction, sender: Runtime.MessageSender, sendResponse: (args: IMessageResponse) => void) => {
            console.log('action : ', msg.action)
            switch (msg.action) {
                case "checkHost":
                    getConfigForHost(msg.args[0]).then(config => sendResponse({
                        success: true,
                        data: config
                    })).catch(error => sendResponse({
                        success: false,
                        error: error
                    }))
                    break
                case "getAllHosts":
                    manager.hosts.getAll().then((results) => sendResponse({
                        success: true,
                        data: results
                    })).catch(error => sendResponse({
                        success: false,
                        error: error
                    }))
                    break
                case "searchSavePage":
                    getCurrentSavePage(msg.args[0]).then(pageEntry => sendResponse({
                        success: true,
                        data: pageEntry
                    })).catch(error => sendResponse({
                        success: false,
                        error: error
                    }))
                    break
                case "updateSavePage":
                    manager.entries.update(msg.args[0], msg.args[1]).then(() => sendResponse({
                        success: true,
                    })).catch(error => sendResponse({
                        success: false,
                        error: error
                    }))
                    break
                case "removePage":
                    manager.entries.delete(msg.args[0]).then(() => sendResponse({
                        success: true,
                    })).catch(error => sendResponse({
                        success: false,
                        error: error
                    }))
                    break
                case "savePage":
                    manager.entries.add(msg.args[0]).then(() => sendResponse({
                        success: true,
                    })).catch(error => sendResponse({
                        success: false,
                        error: error
                    }))
                    break
                case "getPagesWithNextPage":
                    manager.entries.getEntriesWithNextUrl().then((results) => sendResponse({
                        success: true,
                        data: results
                    })).catch(error => sendResponse({
                        success: false,
                        error: error
                    }))
                    break
                case "getPagesWithoutNextPage":
                    manager.entries.getEntriesWithoutNextUrl().then((results) => sendResponse({
                        success: true,
                        data: results
                    })).catch(error => sendResponse({
                        success: false,
                        error: error
                    }))
                    break
                case "refreshPage":
                    refreshEntry(msg.args[0]).then(newEntry => sendResponse({
                    success: true,
                    data: newEntry
                })).catch(error => sendResponse({
                    success: false,
                    error: error
                }))
                    break;
            }
            // return true from the event listener to indicate you wish to send a response asynchronously
            // (this will keep the message channel open to the other end until sendResponse is called).
            // waiting for promise support in chrome
            return true;
        });

        // action display
        // todo : change action display base on page with next url or not (maybe if checking too?)
        // https://developer.chrome.com/docs/extensions/reference/api/action?hl=fr

        // alarms
        browser.alarms.onAlarm.addListener((alarm) => {
            refreshAllEntries().then(() => {

            })
        });

        // notifications gestion
        // todo : send notification whe new next page found

        // analysing page for next url
        // todo : implement page analysing to check for next url validity
    }
});

async function refreshEntry(entry: IEntryData): Promise<IEntryData> {
    const url = new URL(entry.url)
    const config = await getConfigForHost(url.host)
    const hasNextPage = await checkNextPage(entry, config)
    const newEntry = {
        ...entry,
        hasNextUrl: hasNextPage,
        lastCheck: new Date()
    }
    await manager.entries.update(entry, newEntry)
    return newEntry
}

async function refreshAllEntries() {
    const entries = await manager.entries.getEntriesWithoutNextUrl()

    for (const entry of entries) {
        await refreshEntry(entry)
    }
}