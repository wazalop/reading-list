import {createSiteData, isNextUrl, isSameEntry} from "@/lib/utils/PageService"
import {getNextUrl} from "@/lib/parsers/ParserService"
import {sendMessage} from "@/lib/utils/MessagesService";
import AddBtn from "@/lib/components/AddBtn.svelte"
import ConflictBtn from "@/lib/components/ConflictBtn.svelte"
import RemoveBtn from "@/lib/components/RemoveBtn.svelte";
import "@/assets/bookmarks-mangas.css"

export default defineContentScript({
    matches: [
        "http://*/*",
        "https://*/*"
    ],
    async main(ctx) {
        let btnContainer: HTMLElement
        let currentEntry: IEntryData
        let saveEntry: IEntryData
        const page = {
            url: location.href
        }

        const ui = createIntegratedUi(ctx, {
            anchor: document.body,
            position: 'inline',
            onMount(container) {
                // Define how your UI will be mounted inside the container
                btnContainer = document.createElement('div');
                btnContainer.classList.add("bm-container")
                container.append(btnContainer);
            },
        });
        ui.mount();

        function deleteCurrentPage() {
            if (currentEntry) {
                sendMessage<void>({
                    action: "removePage",
                    args: [currentEntry]
                }).then(() => {
                    showAddBtn()
                }).catch((error) => {
                    console.error('error : ', error)
                })
            }
        }

        function updateSaveEntryWithCurrent() {
            if (currentEntry && saveEntry) {
                sendMessage<void>({
                    action: "updateSavePage",
                    args: [saveEntry, currentEntry]
                }).then(() => {
                    showDeleteBtn()
                }).catch((error) => {
                    console.error('error : ', error)
                })
            }
        }

        function goToSaveEntry() {
            if (saveEntry) {
                location.href = saveEntry.url
            }
        }

        function saveCurrentEntry() {
            if (currentEntry) {
                sendMessage<void>({
                    action: "savePage",
                    args: [currentEntry]
                }).then(() => {
                    showDeleteBtn()
                }).catch((error) => {
                    console.error('error : ', error)
                })
            }
        }

        function showDeleteBtn() {
            btnContainer.innerHTML = ""
            new RemoveBtn({
                target: btnContainer
            })

            document.querySelector("#bm-btn")?.addEventListener('click', deleteCurrentPage)
        }

        function showConflictBtn() {
            btnContainer.innerHTML = ""
            new ConflictBtn({
                target: btnContainer
            })

            document.querySelector("#bm-conflict-url")!.textContent = saveEntry?.url

            document.querySelector("#bm-btn-update")?.addEventListener('click', updateSaveEntryWithCurrent)
            document.querySelector("#bm-btn-goto")?.addEventListener('click', goToSaveEntry)
        }

        function showAddBtn() {
            btnContainer.innerHTML = ""
            new AddBtn({
                target: btnContainer
            })

            document.querySelector("#bm-btn")?.addEventListener("click", saveCurrentEntry)
        }

        async function checkCurrentPageStatus() {
            try {
                const hostconfig: IHostSetting = await sendMessage<IHostSetting>({
                    action: "checkHost",
                    args: [location.host]
                })

                const parserReturn = getNextUrl(page, hostconfig);

                currentEntry = createSiteData(page, parserReturn);

                saveEntry = await sendMessage<IEntryData>({
                    action: "searchSavePage",
                    args: [currentEntry]
                })

                if (isSameEntry(saveEntry, currentEntry)) {
                    showDeleteBtn();
                } else {
                    if (isNextUrl(saveEntry, currentEntry)) {
                        await sendMessage<IEntryData>({
                            action: "updateSavePage",
                            args: [saveEntry, currentEntry]
                        })
                        showDeleteBtn();
                    } else {
                        showConflictBtn();
                    }
                }
            } catch (error: any) {
                if (error?._type === "NoSaveDataFound") {
                    showAddBtn();
                }
            }
        }

        checkCurrentPageStatus()
    },
});
