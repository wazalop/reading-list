import {analysers} from "@/lib/analysers";
import {AnalyserError} from "@/lib/errors/AnalyserError";

export async function checkNextPage(page: IEntryData, hostConfig: IHostSetting): Promise<0|1> {
    if (hostConfig?.analyser?.name) {
        const analyserClass = analysers[(hostConfig.analyser.name as keyof typeof analysers)]
        if (analyserClass) {
            return await analyserClass(page) ? 1 : 0
        }
    }

    throw new AnalyserError(hostConfig)
}