import {NewUrlNotFound} from "@/lib/errors/NewUrlNotFound";

export function urlInBodyAnalyser(pageData: IEntryData): Promise<IAnalyserReturn> {
    return new Promise((resolve, reject) => {
        fetch(pageData.nextUrl).then(response => response.text()).then(body => {
            if(body.indexOf(pageData.nextUrl) !== -1) {
                resolve(true)
            }
            reject(new NewUrlNotFound(pageData))
        }).catch(() => {
            reject(new NewUrlNotFound(pageData))
        })
    })
}