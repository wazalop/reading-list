import {urlInBodyAnalyser} from "@/lib/analysers/UrlInBodyAnalyser";

export const analysers = {
    urlInBodyAnalyser: urlInBodyAnalyser
}