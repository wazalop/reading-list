type AnalyserConfig = UrlInBodyanalyser

type UrlInBodyanalyser = {
    name: "urlInBodyanalyser",
    options: {}
}

type IAnalyserReturn = true