export class AnalyserError extends Error {
    private readonly _hostConfig: IHostSetting
    readonly _type: string = "AnalyserError"
    constructor(hostConfig: IHostSetting) {
        let message = `Unknown analyser`
        super(message);
        this._hostConfig = hostConfig;
    }

    get hostConfig(): IHostSetting {
        return this._hostConfig;
    }

    get type(): string {
        return this._type;
    }
}