export class NewUrlNotFound extends Error {
    private readonly _type: string = "NewUrlNotFound"
    _entry: IEntryData
    constructor(entry: IEntryData) {
        const message = `next url not found for ${entry.name}`
        super(message);
        this.name = 'NewUrlNotFound';
        this._entry = entry
    }

    get entry(): IEntryData {
        return this._entry
    }

    get type(): string {
        return this._type;
    }
}