export class NoSaveDataFound extends Error {
    _entry: IEntryData
    private readonly _type: string = "NoSaveDataFound"
    constructor(entry: IEntryData) {
        const message = `No save data found for [${entry.name}]`
        super(message);
        this._entry = entry
    }

    get entry(): IEntryData {
        return this._entry
    }

    get type(): string {
        return this._type;
    }
}