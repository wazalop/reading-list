export class ParserError extends Error {
    private readonly _hostConfig: IHostSetting
    private readonly _type: string = "ParserError"
    constructor(hostConfig: IHostSetting) {
        let message = `Unknown parser`
        super(message);
        this._hostConfig = hostConfig;
    }

    get hostConfig(): IHostSetting {
        return this._hostConfig;
    }

    get type(): string {
        return this._type;
    }
}