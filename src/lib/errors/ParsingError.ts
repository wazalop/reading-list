export class ParsingError extends Error {
    private readonly _page: IPageData
    private readonly _parser: string
    private readonly _type: string = "ParsingError"

    constructor(page: IPageData, parser: string) {
        const message = `Parsin error for [${page.url}] with parser [${parser}]`
        super(message);
        this._page = page
        this._parser = parser
    }

    get page(): IPageData {
        return this._page
    }

    get parser(): string {
        return this._parser;
    }

    get type(): string {
        return this._type;
    }
}