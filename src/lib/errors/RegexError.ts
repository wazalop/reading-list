export class RegexMatchError extends Error {
    private readonly _type: string = "RegexMatchError"
    constructor(parameter: string = ``) {
        const message = `Regex error, the regex didn't return the ${parameter}`
        super(message);
        this.name = 'RegexMatchError';
    }

    get type(): string {
        return this._type;
    }
}