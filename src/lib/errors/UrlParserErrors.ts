export class UrlParserOptionsError extends Error {
    private readonly _type: string = "UrlParserOptionsError"
    constructor(message: string = `UrlParser error, the options is invalid`) {
        super(message);
        this.name = 'UrlParserOptionsError';
    }

    get type(): string {
        return this._type;
    }
}