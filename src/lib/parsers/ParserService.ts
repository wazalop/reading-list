import {ParserError} from "@/lib/errors/ParserError";
import {UrlParserOptionsError} from "@/lib/errors/UrlParserErrors";
import {parsers} from "@/lib/parsers";


export function getNextUrl(page:IPageData, hostConfig: IHostSetting): IParserReturn {
    if(hostConfig?.parser?.name) {
        const parserClass = parsers[(hostConfig.parser.name as keyof typeof parsers)]
        if (parserClass) {
            return parserClass(page, hostConfig.parser.options)
        }
    }

    throw new ParserError(hostConfig)
}

export function incrementChapterNumber(chapter: string) {
    const chapterNumber = chapter.match("[0-9]+")
    if(chapterNumber) {
        const nextChapter = +chapterNumber[0] + 1
        return chapter.replace(chapterNumber[0], String(nextChapter))
    }
    throw new Error("No chapter found")
}

export function checkOptions<T extends ParserOptions>(options: T, requiredOptions: string[]) {
    if(!requiredOptions.every(item => options.hasOwnProperty(item))) throw new UrlParserOptionsError()
}