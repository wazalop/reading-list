import { expect, describe, it } from 'vitest'
import {urlParser} from "@/lib/parsers/UrlParser";
import hosts from "../../../configurations/hosts.json"
import {ParsingError} from "@/lib/errors/ParsingError";

describe('UrlParser', () => {
    it('should throw if no next url found', () => {
        const currentPage: IPageData = {
            url: "https://example.com/page-1",
        };
        const options: UrlParserOptions = {
            urlFormat: "(https:\/\/example.com\/)",
            regexp: "(https:\/\/example.com\/)(page)",
        };
        expect(() => urlParser(currentPage, options)).toThrow(ParsingError);
    });
    it('should throw if bad Config', () => {
        const currentPage: IPageData = {
            url: "https://example.com/page-1",
        };
        const options = {};
        // @ts-ignore
        expect(() => urlParser(currentPage, options)).toThrow();
    });
    it('readmng parser', () => {
        // @ts-ignore
        const config: IHostSetting = hosts[2];
        const currentPage: IPageData = {
            url: "https://www.readmng.com/the-ruler-of-the-land/642"
        };
        expect(urlParser(currentPage, config.parser.options)).toStrictEqual({
            title: "the-ruler-of-the-land",
            chapter: "643",
            url: "https://www.readmng.com/the-ruler-of-the-land/643"
        });
    });
    it('manhuaus parser', () => {
        // @ts-ignore
        const config: IHostSetting = hosts[0];
        const currentPage: IPageData = {
            url: "https://manhuaus.com/manga/star-embracing-swordmaster/chapter-19/",
        };
        expect(urlParser(currentPage, config.parser.options)).toStrictEqual({
            title: "star-embracing-swordmaster",
            chapter: "20",
            url: "https://manhuaus.com/manga/star-embracing-swordmaster/chapter-20/"
        });
    });
    it('toonily parser', () => {
        // @ts-ignore
        const config: IHostSetting = hosts[3];
        const currentPage: IPageData = {
            url: "https://toonily.com/webtoon/mercenary-enrollment/chapter-160/",
        };
        expect(urlParser(currentPage, config.parser.options)).toStrictEqual({
            title: "mercenary-enrollment",
            chapter: "161",
            url: "https://toonily.com/webtoon/mercenary-enrollment/chapter-161/"
        });
    });
    it('topmanhua parser', () => {
        // @ts-ignore
        const config: IHostSetting = hosts[4];
        const currentPage: IPageData = {
            url: "https://topmanhua.com/manhua/adventures-of-an-undead-who-became-paladin/chapter-169/",
        };
        expect(urlParser(currentPage, config.parser.options)).toStrictEqual({
            title: "adventures-of-an-undead-who-became-paladin",
            chapter: "170",
            url: "https://topmanhua.com/manhua/adventures-of-an-undead-who-became-paladin/chapter-170/"
        });
    });
});