import {checkOptions, incrementChapterNumber} from "@/lib/parsers/ParserService";
import {ParsingError} from "@/lib/errors/ParsingError";

export function urlParser(page: IPageData, options: UrlParserOptions): IParserReturn {
    checkOptions<UrlParserOptions>(options, ['urlFormat', 'regexp'])
    const regexp = RegExp(options.regexp);
    const matched = page.url.match(regexp);
    const matchedGroups = matched?.groups;
    if(matchedGroups) {
        const title = matchedGroups.title
        const chapter = matchedGroups.chapter
        const nextChapter = incrementChapterNumber(chapter)
        let url = options.urlFormat.replace("[title]", title);
        url = url.replace("[chapter]", nextChapter)
        return {
            title,
            chapter: nextChapter,
            url
        }
    }
    throw new ParsingError(page, "urlParser")
}