type ParserOptions = {}

type IParserReturn = {
    url: string
    title: string
    chapter: string
}

// Parser types
type ParserConfig = UrlParser

type UrlParser = {
    name: "urlParser",
    options: UrlParserOptions
}
type UrlParserOptions = {
    urlFormat: string
    regexp: string
}