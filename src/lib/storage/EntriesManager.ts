import type {IDBManager, StorageSchema} from "@/lib/storage/StorageManager";

export class EntriesManager {
    private readonly _manager: IDBManager
    private readonly _name = "entries"

    constructor(manager: IDBManager) {
        this._manager = manager

        manager.addEventListener("IDBUpdated", this.upgrade.bind(this))
    }

    private upgrade(event: CustomEvent<IDBUpdated<StorageSchema>>) {
        const objectStore = event.detail.db?.createObjectStore(this._name, {
            keyPath: 'name'
        });
        objectStore?.createIndex("hasNextPage", "hasNextPage", {unique: false})
    }

    getEntriesWithNextUrl() {
        if(this._manager.db) return this._manager.db.getAllFromIndex(this._name, "hasNextPage", 1);
        throw new Error("database not found")
    }

    getEntriesWithoutNextUrl() {
        if(this._manager.db) return this._manager.db.getAllFromIndex(this._name, "hasNextPage", 0);
        throw new Error("database not found")
    }

    getByName(name: string) {
        if(this._manager.db) return this._manager.db.get(this._name, name);
        throw new Error("database not found")
    }
    getAll() {
        if(this._manager.db) return this._manager.db.getAll(this._name);
        throw new Error("database not found")
    }

    async add(entry: IEntryData) {
        if(this._manager.db) return this._manager.db.put(this._name, entry);
        throw new Error("database not found")
    }

    async delete(entry: IEntryData) {
        if(this._manager.db) return this._manager.db.delete(this._name, entry.name);
        throw new Error("database not found")
    }

    async update(saveEntry: IEntryData, entry: IEntryData): Promise<boolean> {
        if(this._manager.db) {
            await this.delete(saveEntry)
            await this.add(entry)
            return true
        }
        throw new Error("database not found")
    }
}