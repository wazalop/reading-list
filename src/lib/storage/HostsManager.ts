import {type IDBManager, type StorageSchema} from "@/lib/storage/StorageManager";

export class HostsManager {
    private readonly _manager: IDBManager
    private readonly _name = "hosts"

    constructor(manager: IDBManager) {
        this._manager = manager

        manager.addEventListener("IDBUpdated", this.upgrade.bind(this))
    }

    private async upgrade(event: CustomEvent<IDBUpdated<StorageSchema>>) {
        event.detail.db?.createObjectStore(this._name, {
            keyPath: "hostname",
        });
    }

    getByHost(host: string) {
        if(this._manager.db) return this._manager.db.get(this._name, host);
        throw new Error("database not found")
    }

    getAll() {
        if(this._manager.db) return this._manager.db.getAll(this._name);
        throw new Error("database not found")
    }

    add(hostSetting: IHostSetting) {
        if(this._manager.db) return this._manager.db.put(this._name, hostSetting);
        throw new Error("database not found")
    }

    delete(hostname: string) {
        if (this._manager.db) return this._manager.db.delete(this._name, hostname);
        throw new Error("database not found")
    }

    clear() {
        if(this._manager.db) return this._manager.db.clear(this._name);
        throw new Error("database not found")
    }

    async saveAll(parsersConfigs: IHostSetting[]) {
        if (this._manager.db) {
            const transaction = this._manager.db?.transaction(this._name, "readwrite")
            await Promise.all(parsersConfigs.map((host) => transaction.store.add(host)));
            return await transaction.done;
        }
        throw new Error("database not found")
    }
}