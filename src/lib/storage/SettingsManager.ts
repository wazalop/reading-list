import type {IDBManager, StorageSchema} from "@/lib/storage/StorageManager"

export enum Configuration {
    ALARM_PERIOD = 'bm-alarm-period',
}

export class SettingsManager {
    private readonly _manager: IDBManager
    private readonly _name = "settings"

    constructor(manager: IDBManager) {
        this._manager = manager

        manager.addEventListener("IDBUpdated", this.upgrade.bind(this))
    }

    private upgrade(event: CustomEvent<IDBUpdated<StorageSchema>>) {
        event.detail.db?.createObjectStore(this._name, {
            keyPath: 'key'
        });
    }


    get(key: string) {
        if(this._manager.db) return this._manager.db.get(this._name, key);
        throw new Error("database not found")
    }

    set(key: string, value: any) {
        if(this._manager.db) return this._manager.db.put(this._name, value, key);
        throw new Error("database not found")
    }

    delete(key: string) {
        if(this._manager.db) return this._manager.db.delete(this._name, key);
        throw new Error("database not found")
    }

    clear() {
        if(this._manager.db) return this._manager.db.clear(this._name);
    }
}