import {SettingsManager} from "@/lib/storage/SettingsManager";
import {EntriesManager} from "@/lib/storage/EntriesManager";
import {HostsManager} from "@/lib/storage/HostsManager";
import {type DBSchema, type IDBPDatabase, openDB} from "idb";

export const SETTINGS: AppSettings = {
    CONFIG_URL: "https://gitlab.com/wazalop/reading-list/-/raw/master/configurations/",
    STORAGE_NAME: "bookmarks-mangas",
    VERSION: 1,
    ALARM_NAME: 'bm-alarm-name',
}

export interface StorageSchema extends DBSchema {
    settings: {
        key: string
        value: any
    }
    hosts: {
        value: IHostSetting
        key: string
    }
    entries: {
        value: IEntryData
        key: string
        indexes: {
            hasNextPage: number
        }
    }
}

interface IDBEventMap {
    IDBUpdated: CustomEvent<IDBUpdated<StorageSchema>>;
    IDBBlocked: CustomEvent<IDBBlocked>;
    IDBBlocking: CustomEvent<IDBBlocking>;
    IDBTerminated: CustomEvent<IDBTerminated>;
    'db:ready': CustomEvent
}

export interface IDBManager extends EventTarget {
    addEventListener<K extends keyof IDBEventMap>(type: K, listener: (this: IDBManager, ev: IDBEventMap[K]) => void, options?: boolean | AddEventListenerOptions): void;
    addEventListener(type: string, listener: EventListenerOrEventListenerObject, options?: boolean | AddEventListenerOptions): void;
}

export class IDBManager extends EventTarget{
    private _db: IDBPDatabase<StorageSchema> | undefined
    private readonly _settings: SettingsManager
    private readonly _entries: EntriesManager
    private readonly _hosts: HostsManager

    constructor() {
        super()
        this._entries = new EntriesManager(this)
        this._settings = new SettingsManager(this)
        this._hosts = new HostsManager(this)
        this.init()
    }

    private async init() {
        this._db = await openDB<StorageSchema>(SETTINGS.STORAGE_NAME, SETTINGS.VERSION, {
            upgrade: (db, oldVersion, newVersion, transaction, event) => {
                this.dispatchEvent(new CustomEvent<IDBUpdated<StorageSchema>>("IDBUpdated", {
                    detail: {
                        db,
                        oldVersion,
                        newVersion,
                        transaction,
                        event,
                    }
                }))
            },
            blocked: (currentVersion, blockedVersion, event) => {
                this.dispatchEvent(new CustomEvent<IDBBlocked>("IDBBlocked", {
                    detail: {
                        currentVersion,
                        blockedVersion,
                        event,
                    }
                }))
            },
            blocking:(currentVersion, blockedVersion, event) => {
                this.dispatchEvent(new CustomEvent<IDBBlocking>("IDBBlocking", {
                    detail: {
                        currentVersion,
                        blockedVersion,
                        event,
                    }
                }))
            },
            terminated: () => {
                this.dispatchEvent(new CustomEvent<IDBTerminated>("IDBTerminated"))
            },
        });

        this.dispatchEvent(new CustomEvent('db:ready'))
    }

    get db() {
        return this._db
    }

    get settings() {
        return this._settings
    }

    get entries() {
        return this._entries
    }

    get hosts() {
        return this._hosts
    }
}

export const manager = new IDBManager()