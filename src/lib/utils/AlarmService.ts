import {manager, SETTINGS} from "@/lib/storage/StorageManager";
import {Configuration} from "@/lib/storage/SettingsManager";

export async function createAlarm() {
    let period = 1
    try {
        period = await manager.settings.get(Configuration.ALARM_PERIOD)
    } catch (e) {
    }
    updateAlarm(period)
}

export async function updateAlarm(period: number) {
    try {
        const alarm = await browser.alarms.get(SETTINGS.ALARM_NAME)
        if(alarm) await browser.alarms.clear(alarm.name)
    } catch (e) {}

    return browser.alarms.create(SETTINGS.ALARM_NAME, {
        delayInMinutes: 1,
        periodInMinutes: period,
    });
}