export function sendMessage<T>(action: IMessageAction, callback?: (args: IMessageResponse) => void): Promise<T> {
    return new Promise((resolve, reject) => {
        // @ts-ignore
        chrome.runtime.sendMessage(action, (response: IMessageResponse) => {
            if(response.success) {
                resolve(response.data);
            } else {
                reject(response.error);
            }
        });
    })
}