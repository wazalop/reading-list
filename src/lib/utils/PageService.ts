import {manager} from "@/lib/storage/StorageManager";
import {NoSaveDataFound} from "@/lib/errors/NoSaveDataFound";

export function createSiteData(page: IPageData, parserReturn: IParserReturn): IEntryData {
    return {
        ...page,
        name: parserReturn.title,
        nextUrl: parserReturn.url,
        hasNextUrl: 0
    }
}

export async function getCurrentSavePage(entry: IEntryData): Promise<IEntryData> {
    const result = await manager.entries.getByName(entry.name)
    if (result) return result
    throw new NoSaveDataFound(entry)
}

export function isSameEntry(entryA: IEntryData, entryB: IEntryData): boolean {
    return entryA.url === entryB.url
}

export function isNextUrl(saveEntry: IEntryData, currentEntry: IEntryData): boolean {
    return saveEntry.nextUrl === currentEntry.url
}

export async function getConfigForHost(hostname: string): Promise<IHostSetting> {
    const result = await manager.hosts.getByHost(hostname)
    if(result) return result
    throw new Error("Host not available")
}