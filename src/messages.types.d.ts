type IMessageAction =
    IMessageActionCheckHost
    | IMessageActionSearchExist
    | IMessageActionUpdatePage
    | IMessageActionGetAllHosts
    | IMessageActionSavePage
    | IMessageActionRemovePage
    | IMessageActionGetPagesWithNextPage
    | IMessageActionGetPagesWithoutNextPage
    | IMessageActionRefreshPages

type IMessageResponse = {
    success: boolean,
    data?: any,
    error?: string
}

type IMessageActionGetAllHosts = {
    action: "getAllHosts"
}

type IMessageActionCheckHost = {
    action: "checkHost",
    args: [string]
}

type IMessageActionSearchExist = {
    action: "searchSavePage",
    args: [IEntryData]
}

type IMessageActionUpdatePage = {
    action: "updateSavePage",
    args: [IEntryData, IEntryData]
}

type IMessageActionSavePage = {
    action: "savePage",
    args: [IEntryData]
}

type IMessageActionRemovePage = {
    action: "removePage",
    args: [IEntryData]
}

type IMessageActionGetPagesWithNextPage = {
    action: "getPagesWithNextPage",
}

type IMessageActionGetPagesWithoutNextPage = {
    action: "getPagesWithoutNextPage",
}

type IMessageActionRefreshPages = {
    action: "refreshPage",
    args: [IEntryData]
}


type IMessageActionRefreshPages = {
    action: "checkNextUrl",
    args: [IEntryData]
}
