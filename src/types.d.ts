// Page types
type IPageData = {
    url: string
}
type IEntryData = {
    name: string;
    url: string;
    nextUrl: string;
    hasNextUrl: 0|1;
    lastCheck?: Date;
}


// DB types
type IDBUpdated<T> = {
    db: IDBPDatabase<T>
    oldVersion: number
    newVersion: number | null
    transaction: IDBPTransaction<T, ArrayLike<string>, "versionchange">
    event: IDBVersionChangeEvent
}
type IDBBlocked = {
    currentVersion: number
    blockedVersion: number | null
    event: IDBVersionChangeEvent
}
type IDBBlocking = {
    currentVersion: number
    blockedVersion: number | null
    event: IDBVersionChangeEvent
}
type IDBTerminated = {
}

// App types
type AppSettings = {
    CONFIG_URL: string
    STORAGE_NAME: string
    VERSION: number
    ALARM_NAME: string
}

// host types
type IHostSetting = {
    hostname: string
    parser: ParserConfig
    analyser: AnalyserConfig
}